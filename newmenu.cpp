#include "newmenu.h"
#include "tutorial.h"
#include "cubegame/CubeApp.h"


//-----------
//helper nonsense
//-----------
ButtonGroupManager::ButtonGroupManager():mLast(-1)
{
	mClickDownSound = G_SOUND_MANAGER.getSound("sounds/CLICK.raw");
	mClickUpSound = G_SOUND_MANAGER.getSound("sounds/CLICK.raw");
	//mClickOffSound = G_SOUND_MANAGER.getSound("sounds/silence.raw");
}
ButtonGroupManager::~ButtonGroupManager()
{
}
void ButtonGroupManager::addButton(QuStupidPointer<QuEnlargeableRelScreenFloatBoxButton> aButton, QuStupidPointer<QuBaseImage> image)
{ mButtonGroup.addButton(aButton,image); }
void ButtonGroupManager::addButton(QuEnlargeableRelScreenFloatBoxButton aButton, QuStupidPointer<QuBaseImage> image)
{ mButtonGroup.addButton(new QuEnlargeableRelScreenFloatBoxButton(aButton),image); }
void ButtonGroupManager::addButton(QuStupidPointer<QuBaseImage> image, GDeviceRotation aRot, QuFVector2<float> pos, float scale, bool offsetToCenter)
{ addButton(QuEnlargeableRelScreenFloatBoxButton::fromImageScreenRelativeScaling(image.getReference(),aRot,pos,scale,offsetToCenter),image); }
void ButtonGroupManager::singleMotion(QuScreenCoord crd, GDeviceRotation aRot)
{ 
	int k = mButtonGroup.click(crd,aRot);
	if(k != mLast && mLast >= 0)
		mButtonGroup.getButton(mLast).enlargeOffset(false);
	else if(mLast >= 0)
		mButtonGroup.getButton(mLast).enlargeOffset(true,0.01f);
}
void ButtonGroupManager::singleDown(QuScreenCoord crd, GDeviceRotation aRot)
{
	mLast = mButtonGroup.click(crd,aRot);
	if(mLast != -1)
	{
		//mClickDownSound->play();
		mButtonGroup.getButton(mLast).enlargeOffset(true,0.01f);
	}
	//std::cout << "down on button " << mLast << std::endl;
}
int ButtonGroupManager::singleUp(QuScreenCoord crd, GDeviceRotation aRot)
{
	//TEMP
	//return -1;

	if(mLast == -1)
		return -1;
	if(mLast == mButtonGroup.click(crd,aRot))
	{
		mClickUpSound->play();
		mButtonGroup.getButton(mLast).enlargeOffset(false);
		return mLast;
	}
	//mClickOffSound->play();
	return -1;
}



//------------------
//STARTUP DIALOG
//------------------
void DiaFirst::initialize()
{
	G_SOUND_MANAGER.getSound("sounds/CLICK.raw")->play();
	BgImageManager::initialize();
	mTitle.setImage(G_IMAGE_MANAGER.getFlyImage("menu/background_02.png"));
	mStaticGroup.addButton(G_IMAGE_MANAGER.getFlyImage("menu/buttons-29.png"),mCamera.getRotation(),QuFVector2<float>(0.03f,0.75f),0.7f,false);
	mButtonGroup.addButton(
		QuEnlargeableRelScreenFloatBoxButton::fromImageScreenRelativeScaling(G_IMAGE_MANAGER.getFlyImage("menu/buttons-13.png").getReference(),mCamera.getRotation(),QuFVector2<float>(0.5f,0.5f),0.35f,true),
		G_IMAGE_MANAGER.getFlyImage("menu/buttons-13.png"));//id 0: to DiaMain
}
void DiaFirst::singleUp(int button, QuScreenCoord crd)
{
	if(mButtonGroup.singleUp(crd, mCamera.getRotation()) == 0)
		G_GAME_GLOBAL.setManagerAndInitializeByTemplate<DiaMain>();
}

//------------------
//DIAMAIN
//goes to DIACREDITS, DIAPLAY, and TutorialCubeManager
//------------------
void DiaMain::initialize()
{
	BgImageManager::initialize();
	mTitle.setImage(G_IMAGE_MANAGER.getFlyImage("menu/background_02.png"));
	mStaticGroup.addButton(G_IMAGE_MANAGER.getFlyImage("menu/buttons-24.png"),mCamera.getRotation(),QuFVector2<float>(0.5f,0.5f),0.35f,true);
	mButtonGroup.addButton(G_IMAGE_MANAGER.getFlyImage("menu/buttons-01.png"),mCamera.getRotation(),QuFVector2<float>(0.03f,0.75f),0.7f,false);
	mButtonGroup.addButton(G_IMAGE_MANAGER.getFlyImage("menu/buttons-02.png"),mCamera.getRotation(),QuFVector2<float>(0.03f,0.5f),0.7f,false);
	mButtonGroup.addButton(G_IMAGE_MANAGER.getFlyImage("menu/buttons-03.png"),mCamera.getRotation(),QuFVector2<float>(0.03f,0.25f),0.7f,false);
	//mButtonGroup.addButton(G_IMAGE_MANAGER.getFlyImage("menu/BACK.png"),mCamera.getRotation(),QuFVector2<float>(0.85f,0.0f),0.15f,false); //id 3: to DiaFirst
	//id 0: to DiaPlay
	//id 1: to TutorialCubeManager
	//id 2: to DiaCredits
}
void DiaMain::singleUp(int button, QuScreenCoord crd)
{
	switch(mButtonGroup.singleUp(crd, mCamera.getRotation()))
	{
		case -1:
			break;
		case 0:
			G_GAME_GLOBAL.setManagerAndInitializeByTemplate<DiaPlay>();
			break;
		case 1:
			G_GAME_GLOBAL.setManagerAndInitializeByTemplate<TutorialCubeManager>();
			break;
		case 2:
			G_GAME_GLOBAL.setManagerAndInitializeByTemplate<DiaCredits>();
			break;
		case 3:
			G_GAME_GLOBAL.setManagerAndInitializeByTemplate<DiaFirst>();
		default:
			break;	
	}
}

//------------------
//DIACREDITS
//goes to DIAMAIN
//------------------
void DiaCredits::initialize()
{
	BgImageManager::initialize();
	mTitle.setImage(G_IMAGE_MANAGER.getFlyImage("menu/background_02.png"));
	mStaticGroup.addButton(G_IMAGE_MANAGER.getFlyImage("menu/buttons-24.png"),mCamera.getRotation(),QuFVector2<float>(0.5f,0.5f),0.35f,true);
	mStaticGroup.addButton(G_IMAGE_MANAGER.getFlyImage("menu/buttons-30.png"),mCamera.getRotation(),QuFVector2<float>(0.25f,0.5f),0.5f,true);
	mStaticGroup.addButton(G_IMAGE_MANAGER.getFlyImage("menu/buttons-31.png"),mCamera.getRotation(),QuFVector2<float>(0.75f,0.5f),0.5f,true);
	mButtonGroup.addButton(G_IMAGE_MANAGER.getFlyImage("menu/BACK.png"),mCamera.getRotation(),QuFVector2<float>(0.85f,0.0f),0.15f,false); //id 0: to DiaMain
}
void DiaCredits::singleUp(int button, QuScreenCoord crd)
{
	if(mButtonGroup.singleUp(crd, mCamera.getRotation()) == 0)
		G_GAME_GLOBAL.setManagerAndInitializeByTemplate<DiaMain>();
}


//------------------
//DiaPlay
//goes to DiaMain, DiaSingleDiff, CubeApp, DiaSpectateFirst
//------------------
void DiaPlay::initialize()
{
	BgImageManager::initialize();
	mTitle.setImage(G_IMAGE_MANAGER.getFlyImage("menu/background_02.png"));
	mStaticGroup.addButton(G_IMAGE_MANAGER.getFlyImage("menu/buttons-24.png"),mCamera.getRotation(),QuFVector2<float>(0.5f,0.5f),0.35f,true);
	mButtonGroup.addButton(G_IMAGE_MANAGER.getFlyImage("menu/buttons-04.png"),mCamera.getRotation(),QuFVector2<float>(0.03f,0.75f),0.7f,false);
	mButtonGroup.addButton(G_IMAGE_MANAGER.getFlyImage("menu/buttons-05.png"),mCamera.getRotation(),QuFVector2<float>(0.03f,0.5f),0.7f,false);
	mButtonGroup.addButton(G_IMAGE_MANAGER.getFlyImage("menu/buttons-06.png"),mCamera.getRotation(),QuFVector2<float>(0.03f,0.25f),0.7f,false);
	mButtonGroup.addButton(G_IMAGE_MANAGER.getFlyImage("menu/BACK.png"),mCamera.getRotation(),QuFVector2<float>(0.85f,0.0f),0.15f,false); //id 3: to DiaMain
}
void DiaPlay::singleUp(int button, QuScreenCoord crd)
{
	switch(mButtonGroup.singleUp(crd, mCamera.getRotation()))
	{
		case -1:
			break;
		case 0:
			G_GAME_GLOBAL.setManagerAndInitializeByTemplate<DiaSingleDiff>();
			break;
		case 1:
			{
				QuStupidPointer<CubeApp> man = new CubeApp();
				man->setAi(-1,-1);
				G_GAME_GLOBAL.setManagerAndInitialize(man.safeCast<QuBaseManager>());
			}
			break;
		case 2:
			G_GAME_GLOBAL.setManagerAndInitializeByTemplate<DiaSpectateFirst>();
			break;
		case 3:
			G_GAME_GLOBAL.setManagerAndInitializeByTemplate<DiaMain>();
			break;
		default:
			break;	
	}
}



//------------------
//DiaSingleDiff
//goes to DiaPlay, DiaSingleFirst
//------------------
void DiaSingleDiff::initialize()
{
	BgImageManager::initialize();
	mTitle.setImage(G_IMAGE_MANAGER.getFlyImage("menu/background_02.png"));
	mStaticGroup.addButton(G_IMAGE_MANAGER.getFlyImage("menu/buttons-24.png"),mCamera.getRotation(),QuFVector2<float>(0.5f,0.5f),0.35f,true);
	mStaticGroup.addButton(G_IMAGE_MANAGER.getFlyImage("menu/buttons-10.png"),mCamera.getRotation(),QuFVector2<float>(0.03f,0.75f),0.7f,false);
	mButtonGroup.addButton(G_IMAGE_MANAGER.getFlyImage("menu/buttons-07.png"),mCamera.getRotation(),QuFVector2<float>(0.2f,0.75f),0.7f,false);
	mButtonGroup.addButton(G_IMAGE_MANAGER.getFlyImage("menu/buttons-08.png"),mCamera.getRotation(),QuFVector2<float>(0.2f,0.5f),0.7f,false);
	mButtonGroup.addButton(G_IMAGE_MANAGER.getFlyImage("menu/buttons-09.png"),mCamera.getRotation(),QuFVector2<float>(0.2f,0.25f),0.7f,false);
	mButtonGroup.addButton(G_IMAGE_MANAGER.getFlyImage("menu/BACK.png"),mCamera.getRotation(),QuFVector2<float>(0.85f,0.0f),0.15f,false); //id 3: to DiaPlay
}

void DiaSingleDiff::singleUp(int button, QuScreenCoord crd)
{
	int k = (mButtonGroup.singleUp(crd, mCamera.getRotation()));
	if(k == 3)
		G_GAME_GLOBAL.setManagerAndInitializeByTemplate<DiaPlay>();
	else if (k != -1)
	{
		//0 easy, 1 med, 2 hard
		G_GAME_GLOBAL.setManagerAndInitialize(QuStupidPointer<DiaSingleFirst>(new DiaSingleFirst(k)).safeCast<QuBaseManager>());
	}
}


//------------------
//DiaSingleFirst
//goes to DiaSingleDiff, CubeApp
//------------------
DiaSingleFirst::DiaSingleFirst(int diff):mDiff(diff){}
void DiaSingleFirst::initialize()
{
	BgImageManager::initialize();
	mTitle.setImage(G_IMAGE_MANAGER.getFlyImage("menu/background_02.png"));
	mStaticGroup.addButton(G_IMAGE_MANAGER.getFlyImage("menu/buttons-24.png"),mCamera.getRotation(),QuFVector2<float>(0.5f,0.5f),0.35f,true);
	mStaticGroup.addButton(G_IMAGE_MANAGER.getFlyImage("menu/buttons-25.png"),mCamera.getRotation(),QuFVector2<float>(0.03f,0.75f),0.7f,false);
	mButtonGroup.addButton(G_IMAGE_MANAGER.getFlyImage("menu/buttons-26.png"),mCamera.getRotation(),QuFVector2<float>(0.3f,0.75f),0.7f,false); //id 0: first
	mButtonGroup.addButton(G_IMAGE_MANAGER.getFlyImage("menu/buttons-27.png"),mCamera.getRotation(),QuFVector2<float>(0.3f,0.5f),0.7f,false); //id 1: second
	mButtonGroup.addButton(G_IMAGE_MANAGER.getFlyImage("menu/BACK.png"),mCamera.getRotation(),QuFVector2<float>(0.85f,0.0f),0.15f,false); //id 2: to DiaSingleDiff
}

void DiaSingleFirst::singleUp(int button, QuScreenCoord crd)
{
	int k = (mButtonGroup.singleUp(crd, mCamera.getRotation()));
	if(k == 2)
		G_GAME_GLOBAL.setManagerAndInitializeByTemplate<DiaSingleDiff>();
	else if (k != -1)
	{
		{
			QuStupidPointer<CubeApp> man = new CubeApp();
			if(k == 0)
				man->setAi(-1,mDiff);
			else man->setAi(mDiff,-1);
			G_GAME_GLOBAL.setManagerAndInitialize(man.safeCast<QuBaseManager>());
		}
	}
}



//------------------
//DiaSpectateFirst
//goes to DiaPlay, CubeApp
//------------------
void DiaSpectateFirst::initialize()
{
	BgImageManager::initialize();
	mTitle.setImage(G_IMAGE_MANAGER.getFlyImage("menu/background_02.png"));
	mStaticGroup.addButton(G_IMAGE_MANAGER.getFlyImage("menu/buttons-24.png"),mCamera.getRotation(),QuFVector2<float>(0.5f,0.5f),0.35f,true);
	mStaticGroup.addButton(G_IMAGE_MANAGER.getFlyImage("menu/buttons-12.png"),mCamera.getRotation(),QuFVector2<float>(0.2f,0.95f),0.4f,false);
	mStaticGroup.addButton(G_IMAGE_MANAGER.getFlyImage("menu/buttons-10.png"),mCamera.getRotation(),QuFVector2<float>(0.03f,0.70f),0.7f,false);
	mButtonGroup.addButton(G_IMAGE_MANAGER.getFlyImage("menu/buttons-07.png"),mCamera.getRotation(),QuFVector2<float>(0.2f,0.70f),0.7f,false);
	mButtonGroup.addButton(G_IMAGE_MANAGER.getFlyImage("menu/buttons-08.png"),mCamera.getRotation(),QuFVector2<float>(0.2f,0.45f),0.7f,false);
	mButtonGroup.addButton(G_IMAGE_MANAGER.getFlyImage("menu/buttons-09.png"),mCamera.getRotation(),QuFVector2<float>(0.2f,0.20f),0.7f,false);
	mButtonGroup.addButton(G_IMAGE_MANAGER.getFlyImage("menu/BACK.png"),mCamera.getRotation(),QuFVector2<float>(0.85f,0.0f),0.15f,false); //id 3: to DiaMain
}

void DiaSpectateFirst::singleUp(int button, QuScreenCoord crd)
{
	int k = (mButtonGroup.singleUp(crd, mCamera.getRotation()));
	if(k == 3)
		G_GAME_GLOBAL.setManagerAndInitializeByTemplate<DiaPlay>();
	else if (k != -1)
	{
		//0 easy, 1 med, 2 hard
		G_GAME_GLOBAL.setManagerAndInitialize(QuStupidPointer<DiaSpectateSecond>(new DiaSpectateSecond(k)).safeCast<QuBaseManager>());
	}
}

//------------------
//DiaSpectateSecond
//goes to DiaPlay, CubeApp
//------------------
DiaSpectateSecond::DiaSpectateSecond(int aDiff):mDiff(aDiff){}
void DiaSpectateSecond::initialize()
{
	BgImageManager::initialize();
	mTitle.setImage(G_IMAGE_MANAGER.getFlyImage("menu/background_02.png"));
	mStaticGroup.addButton(G_IMAGE_MANAGER.getFlyImage("menu/buttons-24.png"),mCamera.getRotation(),QuFVector2<float>(0.5f,0.5f),0.35f,true);
	mStaticGroup.addButton(G_IMAGE_MANAGER.getFlyImage("menu/buttons-12.png"),mCamera.getRotation(),QuFVector2<float>(0.2f,0.95f),0.4f,false);
	mStaticGroup.addButton(G_IMAGE_MANAGER.getFlyImage("menu/buttons-28.png"),mCamera.getRotation(),QuFVector2<float>(0.03f,0.70f),0.7f,false);
	mButtonGroup.addButton(G_IMAGE_MANAGER.getFlyImage("menu/buttons-07.png"),mCamera.getRotation(),QuFVector2<float>(0.2f,0.70f),0.7f,false); //id 0: easy
	mButtonGroup.addButton(G_IMAGE_MANAGER.getFlyImage("menu/buttons-08.png"),mCamera.getRotation(),QuFVector2<float>(0.2f,0.45f),0.7f,false); //id 1: medium
	mButtonGroup.addButton(G_IMAGE_MANAGER.getFlyImage("menu/buttons-09.png"),mCamera.getRotation(),QuFVector2<float>(0.2f,0.20f),0.7f,false); //id 2: hard
	mButtonGroup.addButton(G_IMAGE_MANAGER.getFlyImage("menu/BACK.png"),mCamera.getRotation(),QuFVector2<float>(0.85f,0.0f),0.15f,false); //id 3: to DiaSpectateFirst
}

void DiaSpectateSecond::singleUp(int button, QuScreenCoord crd)
{
	int k = (mButtonGroup.singleUp(crd, mCamera.getRotation()));
	if(k == 3)
		G_GAME_GLOBAL.setManagerAndInitializeByTemplate<DiaSpectateFirst>();
	else if (k != -1)
	{
		{
			QuStupidPointer<CubeApp> man = new CubeApp();
			man->setAi(mDiff,k);
			G_GAME_GLOBAL.setManagerAndInitialize(man.safeCast<QuBaseManager>());
		}
	}
}