#include "tutorial.h"
#include "cubegame\CubeApp.h"
#include "newmenu.h"

void TutorialCubeManager::cubeinit()
{
	mProf2.createAi(0);
	mCam = new QuCamera(SCREEN_WIDTH,SCREEN_HEIGHT);
	mCube = new QuCube();
	mDraw = new CubeDrawer();
	mBg = new QuBackground();
	mTouchEffect = new CubeTouchEffect();
	mFireworks = new CubeFireworks();
	mOverlay = new QuOverlayer();
	mGameRestartTimer.setTargetAndReset(120);
	//sound already loaded from main game..



	//initialize tutorial
	for(int i = 0; i < 5; i++)
	{
		std::stringstream ss;
		ss << "tutorial/ingametut-0" << i + 1 << ".png";
		mTutorial.addTutorialText(G_IMAGE_MANAGER.getFlyImage(ss.str()),QuVector2<int>(230,100));
	}
	mTutorial.setState(CUBETUT_MARK_MOVE);
	mTutSound = G_SOUND_MANAGER.getSound("sounds/SELECT.raw");
}


void TutorialCubeManager::update()
{
	//tutorial
	mTutorial.update();


	mBg->setState(mCube->getCurrentState());
	mBg->update();
	mCam->update();
	mOverlay->update();
	mFireworks->update();
	mAccel.update(); 
	mMouse.update();
	tiltUpdate();

	//decide if we need to do our ai business
	if(mProf1.isAi())
		mProf1.getAi().update(*mCube);
	if(mProf2.isAi())
		mProf2.getAi().update(*mCube);


	//update rotation if ai has moved
	if(mCam->rotateInDirection(mCube->getAndResetLastTurnDirection()))
	{
		if(mTutorial.getState() == CUBETUT_OPPONENT_TURN)
			advanceTutorial(CUBETUT_THREE_WIN);
	}
	mOverlay->setOverlay(mCube->getCurrentPlayer());
	if(mCube->getCurMoveType() == 2)
	{
		if(mGameRestartTimer.getTimeSinceStart() == 1)
		{
			advanceTutorial(CUBETUT_ENJOY);
			mFireworks->makeFireworks(mCube->getCurrentPlayer());
			//G_SOUND_MANAGER.getSound(SOUND_BG)->setFade(30,0);
		}
		//mCam->setViewportHeight(  SCREEN_HEIGHT/float(1-mGameRestartTimer.getLinear()) );
		mGameRestartTimer.update();
		if(mGameRestartTimer.isExpired())
		{
			//TODO fade back to main menu or some shit like that
			mGameRestartTimer.reset();
			//TODO goto main menu
			G_GAME_GLOBAL.setManagerAndInitializeByTemplate<DiaFirst>();
		}
	}
	draw();
}

void TutorialCubeManager::draw()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	mBg->draw();
	mFireworks->draw();
	mCam->setHeightAndDistanceFromRotation();
	mCam->setCubeModelProjection();
	mDraw->computeFrontFacingFaces(*mCam);
	mDraw->drawCube(*mCube,mCube->getCurrentFace());

	//HAAAACK FIX for the QuTimer issue triggered bug... of course you could also go into the overlayer class and add the clamp to the timer...
	if(mTutorial.getState() != CUBETUT_MARK_MOVE)
		mOverlay->draw();

	mTutorial.draw();
}

void TutorialCubeManager::mouseDragged(int x, int y, int button)
{
	mMouse.mouseMoved(x,y);
}

void TutorialCubeManager::mousePressed(int x, int y, int button)
{
	mMouse.mousePressed(x,y);
	mCam->setCubeModelProjection();
	glClear(GL_COLOR_BUFFER_BIT);
	if(mDraw->drawSelectionFaces(mCube->getCurrentFace(),x,y,*mCam) != 13)
		mCam->addImpulse(SCREEN_WIDTH/2 - x,SCREEN_HEIGHT/2 - y);
}

void TutorialCubeManager::mouseReleased(int x, int y, int button)
{
	//TODO pipe input into tutorial overlay so it can advance instructions
	mCam->setCubeModelProjection();
	if( (!mProf1.isAi() && mCube->getCurrentPlayer() == 0) ||
		(!mProf2.isAi() && mCube->getCurrentPlayer() == 1))
	{
		//if mouse has not moved too far from original position
		if(mMouse.getChangeNorm() < 10)
		{
			//if(mCube->getCurMoveType() == 0)
			{
				glClear(GL_COLOR_BUFFER_BIT);
				unsigned face = mDraw->drawSelectionFaces(mCube->getCurrentFace(),x,y,*mCam);
				if(face == 13 && mCube->getCurMoveType() == 1)
				{
					G_SOUND_MANAGER.getSound(SOUND_HIGHLIGHT)->play();
					mCube->undoLastMove();
				}
				else if(face != 13 && mCube->getLastMove() != face)
				{
					if( mCube->getCurMoveType() == 1 )
						mCube->undoLastMove();
					if(mCube->attempMakeMove(face))
					{
						//advance tutorial
						if(mTutorial.getState() == CUBETUT_MARK_MOVE)
							advanceTutorial(CUBETUT_SWIPE);
					}
				}
			}
		}
		QuTurnDirections dir = mMouse.mouseReleased(x,y);
		if(dir != IDENTITY_DIRECTION && !mCube->attemptTurnToFace(dir))
		{
			G_SOUND_MANAGER.getSound(SOUND_ERROR)->play();
			float mag = 2000;
			switch(dir)
			{
			case LEFT:
				mCam->addImpulse(-mag,0);
				break;
			case RIGHT:
				mCam->addImpulse(mag,0);
				break;
			case UP:
				mCam->addImpulse(0,-mag);
				break;
			case DOWN:
				mCam->addImpulse(0,mag);
				break;
			default:
				break;
			}
		}
		else
		{
			if(dir != IDENTITY_DIRECTION)
				if(mTutorial.getState() == CUBETUT_SWIPE)
						advanceTutorial(CUBETUT_OPPONENT_TURN);
		}
		mCam->rotateInDirection(mCube->getAndResetLastTurnDirection());
	}
}

void TutorialCubeManager::tiltUpdate()
{
	float y = s3eAccelerometerGetY()/1000.0;
	float x = s3eAccelerometerGetX()/1000.0;
	//if(G_DEVICE_ROTATION == G_DR_180)
	if(s3eSurfaceGetInt(S3E_SURFACE_DEVICE_BLIT_DIRECTION) == S3E_SURFACE_BLIT_DIR_ROT180)
	{
		x*=-1; y*=-1;
	}
	mCam->setTiltTarget(y,x,s3eAccelerometerGetZ()/1000.0);	
	mCam->adjustTiltTarget(-mMouse.getChange().x/30.,mMouse.getChange().y/40.);


	//mCam->setTiltTarget(mAccel.getY(),mAccel.getX());
	//mCam->adjustTiltTarget(-mMouse.getChange().x/100.,mMouse.getChange().y/200.);
}
