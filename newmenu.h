#pragma once

#include "cubegame/QuCube.h"
#include "cubegame/QuCubeCamera.h"
#include "QuDrawer.h"
#include "cubegame/QuCubeInterface.h"
#include "cubegame/QuAiPlayer.h"
#include "cubegame/CubeDrawer.h"

//new stuff
#include "QuGameManager.h"
#include "QuClutils.h"
#include "QuUtils.h"
#include "QuButton.h"

//hack
#include "s3eKeyboard.h"

class ButtonGroupManager
{
public: //bleah, just easier this way.
	QuStupidPointer<QuSoundStructInterface> mClickDownSound; //plays when click down on button
	QuStupidPointer<QuSoundStructInterface> mClickUpSound; //plays when click up on button
	QuStupidPointer<QuSoundStructInterface> mClickOffSound; //plays when click down on button
	int mLast;
	QuButtonList<QuEnlargeableRelScreenFloatBoxButton> mButtonGroup;
public:
	ButtonGroupManager();
	~ButtonGroupManager();
	QuButtonList<QuEnlargeableRelScreenFloatBoxButton> & getButtonGroup() { return mButtonGroup; }
	void addButton(QuStupidPointer<QuEnlargeableRelScreenFloatBoxButton> aButton, QuStupidPointer<QuBaseImage> image = NULL);
	void addButton(QuEnlargeableRelScreenFloatBoxButton aButton, QuStupidPointer<QuBaseImage> image = NULL);
	void addButton(QuStupidPointer<QuBaseImage> image, GDeviceRotation aRot, QuFVector2<float> pos = QuVector2<float>(0,0), float scale = 1, bool offsetToCenter = true);
	void singleMotion(QuScreenCoord crd, GDeviceRotation aRot);
	void singleDown(QuScreenCoord crd, GDeviceRotation aRot);
	int singleUp(QuScreenCoord crd, GDeviceRotation aRot);
};

class  BgImageManager : public QuBaseManager
{
protected:
	QuImageDrawObject mTitle; //always drawn in the background
	QuBasicCamera mCamera;
	ButtonGroupManager mButtonGroup;
	ButtonGroupManager mStaticGroup;	//cheating heer, buttongroupmanager has more functionality than we need, we just use it to help out with rendering...
	BgImageManager():mCamera(G_SCREEN_WIDTH,G_SCREEN_HEIGHT,G_SCREEN_HEIGHT){}
	virtual ~BgImageManager() 
	{
	}
	void initialize()
	{ 
		if(G_GAME_GLOBAL.isLandscapeOriented())
			mCamera.setRotation(G_DR_0);
		else
			mCamera.setRotation(G_DR_270);	//hack
	}
	void drawBackground()
	{
		//hack
		mCamera.setRotation((GDeviceRotation)DEVICE_ROTATION_TO_INVERSE[(int)G_DEVICE_ROTATION],G_RESTRICT_LANDSCAPE);
		//mCamera.setRotation(G_DEVICE_ROTATION,G_RESTRICT_LANDSCAPE);
		glPushMatrix();
		mCamera.setScene();
		QuVector2<int> dim = G_GAME_GLOBAL.getRotatedScreenDimensions(mCamera.getRotation());
		glScalef(dim.x,dim.y,1);
		glDisable(GL_LIGHTING);
		mTitle.autoDraw();
		glPopMatrix();
	}
	virtual void singleDown(int button, QuScreenCoord crd)
	{
		//std::cout << crd.getFPosition().x << " " << crd.getFPosition().y << std::endl;
		mButtonGroup.singleDown(crd,mCamera.getRotation());
	}
	virtual void singleMotion(QuScreenCoord crd)
	{
		mButtonGroup.singleMotion(crd,mCamera.getRotation());
	}
	virtual void update()
	{
		glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
		drawBackground();
		mStaticGroup.getButtonGroup().drawAllButtonsWithImages(mCamera.getRotation());
		mButtonGroup.getButtonGroup().drawAllButtonsWithImages(mCamera.getRotation());
		s3eDeviceBacklightOn();
	}
};


class DiaFirst : public BgImageManager
{
public:
	void initialize();
	void singleUp(int button, QuScreenCoord crd);
	void keyUp(int key){if(key == s3eKeyAbsBSK) G_GAME_GLOBAL.setQuit();}

};


class DiaMain : public BgImageManager
{
public:
	void initialize();
	void singleUp(int button, QuScreenCoord crd);
	void keyUp(int key){if(key == s3eKeyAbsBSK) G_GAME_GLOBAL.setQuit();}
};

class DiaCredits : public BgImageManager
{
public:
	void initialize();
	void singleUp(int button, QuScreenCoord crd);
	void keyUp(int key){if(key == s3eKeyAbsBSK) G_GAME_GLOBAL.setManagerAndInitializeByTemplate<DiaMain>();}
};

class DiaPlay : public BgImageManager
{
public:
	void initialize();
	void singleUp(int button, QuScreenCoord crd);
	void keyUp(int key){if(key == s3eKeyAbsBSK) G_GAME_GLOBAL.setManagerAndInitializeByTemplate<DiaMain>();}
};

class DiaSingleDiff : public BgImageManager
{
public:
	void initialize();
	void singleUp(int button, QuScreenCoord crd);
	void keyUp(int key){if(key == s3eKeyAbsBSK) G_GAME_GLOBAL.setManagerAndInitializeByTemplate<DiaPlay>();}
};

class DiaSingleFirst : public BgImageManager
{
	int mDiff;
public:
	DiaSingleFirst(int diff);
	void initialize();
	void singleUp(int button, QuScreenCoord crd);
	void keyUp(int key){if(key == s3eKeyAbsBSK) G_GAME_GLOBAL.setManagerAndInitializeByTemplate<DiaSingleDiff>();}
};

class DiaSpectateFirst : public BgImageManager
{
public:
	void initialize();
	void singleUp(int button, QuScreenCoord crd);
	void keyUp(int key){if(key == s3eKeyAbsBSK) G_GAME_GLOBAL.setManagerAndInitializeByTemplate<DiaPlay>();}
};

class DiaSpectateSecond : public BgImageManager
{
	int mDiff;
public:
	DiaSpectateSecond(int aDiff);
	void initialize();
	void singleUp(int button, QuScreenCoord crd);
	void keyUp(int key){if(key == s3eKeyAbsBSK) G_GAME_GLOBAL.setManagerAndInitializeByTemplate<DiaSpectateFirst>();}
};