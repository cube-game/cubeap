#pragma once


#include "cubegame/QuCube.h"
#include "cubegame/QuCubeCamera.h"
#include "QuDrawer.h"
#include "cubegame/QuCubeInterface.h"
#include "cubegame/QuAiPlayer.h"
#include "cubegame/CubeDrawer.h"
#include "QuGameManager.h"
#include "QuClutils.h"
#include "QuUtils.h"
#include "QuButton.h"
#include "QuImage.h"
#include "newmenu.h"

class TutorialOverlayer
{
	unsigned mState;
	unsigned mTargetState;
	QuTimer mEnterDelayTimer; //used so that tutorial text does not get disabled right away on accident
	QuTimer mFadingTimer;
	QuTimer mTextButtonFadingTimer;
	QuBasicCamera mCamera;
	bool mFading;
	QuEnlargeableAbsScreenIntBoxButton mTextButton;
	QuStupidPointer<QuBaseImage> mTextButtonImage;

	std::vector<QuEnlargeableAbsScreenIntBoxButton> mTutorialButtons;
	std::vector<QuStupidPointer<QuBaseImage> > mTutorialImages;

private:
	bool isTextButtonDisplayed()
	{
		return (mFading && mTargetState == mState);
	}
public:
	TutorialOverlayer(): mCamera(G_SCREEN_WIDTH,G_SCREEN_HEIGHT,G_SCREEN_HEIGHT),
		mEnterDelayTimer(0,15), //at 30fps, this means half a second
		mFadingTimer(0,15),
		mTextButtonFadingTimer(0,5),
		mState(-1)
	{
		mCamera.setRotation(G_DR_90);
		mTextButtonImage = G_IMAGE_MANAGER.getFlyImage("images/image.png");
		mTextButton.setBox(QuBox2<int>(20,20,64,64));
	}

	void addTutorialText(QuStupidPointer<QuBaseImage> aImage, QuVector2<int> center)
	{
		mTutorialImages.push_back(aImage);
		//TODO need to calculate x,y position
		center.x -= aImage->getWidth()/2;
		center.y -= aImage->getHeight()/2;
		mTutorialButtons.push_back(QuEnlargeableAbsScreenIntBoxButton(aImage.getConstReference(),center));
	}

	void update()
	{
		//if prev image is faded out, go to next image
		if(mFadingTimer.getLinear() == 0 && mTargetState != mState)
		{
			mState = mTargetState;
			mEnterDelayTimer.reset();
			mFading = false;
		}

		//determine if we want to fade in the text button
		if(isTextButtonDisplayed())
			mTextButtonFadingTimer++;
		else mTextButtonFadingTimer--;
		mTextButtonFadingTimer.clamp();

		//update yo timers
		mEnterDelayTimer++;
		if(mFading)
			mFadingTimer--;
		else mFadingTimer++;
		mFadingTimer.clamp();
	}

	void draw()
	{
		QuImageDrawObject d, e;

		glPushMatrix();
		mCamera.setRotation(G_DEVICE_ROTATION,G_RESTRICT_LANDSCAPE);
		mCamera.setScene();
		glDisable(GL_LIGHTING);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glDisable(GL_DEPTH_TEST);
		glTranslatef(-mCamera.getRotatedWidth()/2.0f,-mCamera.getRotatedHeight()/2.0f,0);

		//glPushMatrix();
		//first draw the tutorial question button thing
		d.setImage(mTextButtonImage);
		d.loadColor(QuColor(1,1,1,mTextButtonFadingTimer.getLinear()));
		d.loadBoxVertices(mTextButton.getBox());
		d.autoDraw();
		//glPopMatrix();

		//next draw whichiever button is active
		if(mState != -1)
		{
			e.setImage(mTutorialImages[mState]);
			e.loadColor(QuColor(1,1,1,mFadingTimer.getLinear()));
			e.loadBoxVertices(mTutorialButtons[mState].getBox());
			QuBox2<int> maeua = mTutorialButtons[mState].getBox();
			e.autoDraw();
		}

		glPopMatrix();

		glEnable(GL_DEPTH_TEST);
	}

	//-----------
	//accessors
	//-----------
	unsigned getState()
	{
		return mState;
	}

	//-----------
	//use it
	//-----------
	void deactivateTutorial()
	{
		mFading = true;
	}
	void activateTutorial()
	{
		mFading = false;
	}
	void setState(unsigned aState)
	{
		if(aState != mTargetState)
		{
			mFading = true; //fade out current
			mTargetState = aState;
		}
	}
	bool clickTextButton(const QuScreenCoord & crd)
	{
		if(isTextButtonDisplayed())
			if(mTextButton.click(crd,mCamera.getRotation()))
				return true;
		return false;
	}
};

enum TutorialStateEnum
{
	CUBETUT_MARK_MOVE = 0,
	CUBETUT_SWIPE = 1,
	CUBETUT_OPPONENT_TURN = 2,
	CUBETUT_THREE_WIN = 3,
	CUBETUT_ENJOY = 4,

	CUBETUT_DEFAULT = 99
};

class TutorialCubeManager : public QuBaseManager
{
private:
	QuCamera * mCam;
	QuCube * mCube;
	CubeDrawer * mDraw;
	QuBackground * mBg;
	CubeTouchEffect * mTouchEffect;
	QuOverlayer * mOverlay;
	CubeFireworks * mFireworks;

	QuAccelSimulator mAccel;
	QuAccelManager mAccelMan;
	QuMouseDragged mMouse;

	QuPlayerProfile mProf1;
	QuPlayerProfile mProf2;

	QuTimer mGameRestartTimer;

	TutorialOverlayer mTutorial;
	QuStupidPointer<QuSoundStructInterface> mTutSound;

public:
	TutorialCubeManager():mProf1(0),mProf2(1),mCam(NULL)
	{
	}
	~TutorialCubeManager()
	{
		destroy();
	}
	void destroy()
	{
		//as all these things are initialized at the same time, we only need to check if one is initilaized or not
		if(mCam != NULL)
		{
			delete mCam;
			delete mCube;
			delete mDraw;
			delete mBg;
			delete mTouchEffect;
			delete mOverlay;
			delete mFireworks;
		}
	}

	void advanceTutorial(TutorialStateEnum state = CUBETUT_DEFAULT)
	{
		if(state = CUBETUT_DEFAULT)
			mTutorial.setState(mTutorial.getState() + 1);
		else
			mTutorial.setState(state);
		//mTutSound->play();
	}

	void initialize(){cubeinit();}
	void cubeinit();

	void update();
	void draw();

	void keyPressed(int key){}
	void keyReleased(int key)
	{
		if(key == s3eKeyAbsBSK)
			G_GAME_GLOBAL.setManagerAndInitializeByTemplate<DiaFirst>();
	}
	void mouseMoved(int x, int y){} // replaced by mouse dragged, w/e
	void mouseDragged(int x, int y, int button);
	void mousePressed(int x, int y, int button);
	void mouseReleased(int x, int y, int button);

	

	void tiltUpdate();

	virtual void keyDown(int key){keyPressed(key);}
	virtual void keyUp(int key){keyReleased(key);}
	virtual void singleDown(int button, QuScreenCoord crd)
	{ 
		//tutorial is up to date and uses the quscreencoord system
		if(mTutorial.clickTextButton(crd)) 
			mTutorial.activateTutorial(); 
		else if (mTutorial.getState() != CUBETUT_MARK_MOVE && mTutorial.getState() != CUBETUT_SWIPE) //these go by too fast
			;//mTutorial.deactivateTutorial();
		crd.reflect(G_SCREEN_REFLECT_VERTICAL); 
		QuVector2<int> pos = crd.getIPosition(G_DR_0); 
		mousePressed(pos.x,pos.y,button);
	}
	virtual void singleUp(int button, QuScreenCoord crd){ crd.reflect(G_SCREEN_REFLECT_VERTICAL); QuVector2<int> pos = crd.getIPosition(G_DR_0); mouseReleased(pos.x,pos.y,button);}
	virtual void singleMotion(QuScreenCoord crd){ crd.reflect(G_SCREEN_REFLECT_VERTICAL); QuVector2<int> pos = crd.getIPosition(G_DR_0); mouseDragged(pos.x,pos.y,0);}
};
