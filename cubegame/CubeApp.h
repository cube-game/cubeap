
#pragma once
#include "QuCube.h"
#include "QuCubeCamera.h"
#include "QuDrawer.h"
#include "QuCubeInterface.h"
#include "QuAiPlayer.h"
#include "CubeDrawer.h"

//new stuff
#include "QuGameManager.h"
#include "QuClutils.h"
#include "QuUtils.h"

//stupid
int32 pause(void *systemData, void *userData);

class CubeApp : public QuBaseManager
{
private:
	QuCamera * mCam;
	QuCube * mCube;
	CubeDrawer * mDraw;
	QuBackground * mBg;
	QuMenu * mMenu;
	CubeTouchEffect * mTouchEffect;
	QuOverlayer * mOverlay;
	CubeFireworks * mFireworks;

	QuAccelSimulator mAccel;
	QuAccelManager mAccelMan;
	QuMouseDragged mMouse;

	QuPlayerProfile mProf1;
	QuPlayerProfile mProf2;

	QuTimer mGameRestartTimer;

	int hasLoaded;
public:
	bool isDone;
	CubeApp():mProf1(0),mProf2(1),hasLoaded(0),isDone(false),mCam(NULL)
	{
	}
	~CubeApp()
	{
	}
	void destroy()
	{
		//as all these things are initialized at the same time, we only need to check if one is initilaized or not
		if(mCam != NULL)
		{
			delete mCam;
			delete mCube;
			delete mDraw;
			delete mBg;
			delete mOverlay;
			delete mFireworks;
		}
	}
	
	//-1 : human, 0, 1, 2 : easy, med, hard resp.
	void setAi(int p1, int p2)
	{
		
		if(p1 == -1)
			mProf1.reset();
		else
			mProf1.createAi(p1);
		if(p2 == -1)
			mProf2.reset();
		else
			mProf2.createAi(p2);
	}

	void drawGame();
	void updateGame();
	void releaseGame(int x, int y);
	void pressGame(int x, int y);

	void cubeinit();
	void initialize(){ cubeinit(); }

	void update();
	void draw();

	void keyPressed  (int key);
	void keyReleased(int key);
	void mouseMoved(int x, int y );
	void mouseDragged(int x, int y, int button){}
	void mousePressed(int x, int y, int button);
	void mouseReleased(int x, int y, int button);
	void windowResized(int w, int h);

	void tiltUpdate();

	//new
	virtual void keyDown(int key){keyPressed(key);}
	virtual void keyUp(int key){keyReleased(key);}

	//TODO fix this peter...
	virtual void singleDown(int button, QuScreenCoord crd){ crd.reflect(G_SCREEN_REFLECT_VERTICAL); QuVector2<int> pos = crd.getIPosition(G_DR_0); mousePressed(pos.x,pos.y,button);}
	virtual void singleUp(int button, QuScreenCoord crd){ crd.reflect(G_SCREEN_REFLECT_VERTICAL); QuVector2<int> pos = crd.getIPosition(G_DR_0); mouseReleased(pos.x,pos.y,button);}
	virtual void singleMotion(QuScreenCoord crd){ crd.reflect(G_SCREEN_REFLECT_VERTICAL); QuVector2<int> pos = crd.getIPosition(G_DR_0); mouseMoved(pos.x,pos.y);}
};