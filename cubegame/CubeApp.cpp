

#include "CubeApp.h"
#include "QuDrawer.h"
#include "QuCube.h"
#include "QuCubeInterface.h"
#include "QuConstants.h"
#include "QuCubeConstants.h"
#include "s3eKeyboard.h"
#include "substitute.h"
#include "QuModels.h"
#include "CubeDrawer.h"
//new
#include "QuGlobals.h"
#include "../tutorial.h"
#include "../newmenu.h"

void CubeApp::cubeinit()
{
	mCam = new QuCamera(SCREEN_WIDTH,SCREEN_HEIGHT);
	mCube = new QuCube();
	mDraw = new CubeDrawer();
	mBg = new QuBackground();
	mFireworks = new CubeFireworks();
	mOverlay = new QuOverlayer();
	mGameRestartTimer.setTargetAndReset(180);

	G_SOUND_MANAGER.loadSound(SOUND_NEXT_TURN);
	G_SOUND_MANAGER.loadSound(SOUND_WINNER);
	G_SOUND_MANAGER.loadSound(SOUND_HIGHLIGHT);
	G_SOUND_MANAGER.loadSound(SOUND_CLICK);
	G_SOUND_MANAGER.loadSound(SOUND_ERROR);
	G_SOUND_MANAGER.loadSound(SOUND_FIRE);
	G_SOUND_MANAGER.loadSound(SOUND_SELECT);

}

void CubeApp::drawGame()
{
	mBg->draw();
	mFireworks->draw();
	mCam->setHeightAndDistanceFromRotation();
	mCam->setCubeModelProjection();
	mDraw->computeFrontFacingFaces(*mCam);
	mDraw->drawCube(*mCube,mCube->getCurrentFace());
	mOverlay->draw();
	//mDraw->drawSelectionFaces(mCube->getCurrentFace(),0,0);
}

void CubeApp::updateGame()
{
	mBg->setState(mCube->getCurrentState());
	mBg->update();
	mCam->update();
	mOverlay->update();
	mFireworks->update();
	mAccel.update(); 
	mMouse.update();
	tiltUpdate();

	//decide if we need to do our ai business
	if(mProf1.isAi())
		mProf1.getAi().update(*mCube);
	if(mProf2.isAi())
		mProf2.getAi().update(*mCube);

	//update rotation if ai has moved
	mCam->rotateInDirection(mCube->getAndResetLastTurnDirection());

	//if turn has changed, tell qu overlayer to do its business
	mOverlay->setOverlay(mCube->getCurrentPlayer());

	//decide if we want to restart the game
	if(mCube->getCurMoveType() == 2)
	{
		//fade out the music
		//if(!QuSoundManager::getRef().isFadingOut(SOUND_BG))
		//	QuSoundManager::getRef().setFade(SOUND_BG,false);
		if(mGameRestartTimer.getTimeSinceStart() == 1)
		{
			mFireworks->makeFireworks(mCube->getCurrentPlayer());
			//new TODO
			//G_SOUND_MANAGER.getSound(SOUND_BG)->setFade(30,0);
		}
		mGameRestartTimer.update();
		if(mGameRestartTimer.isExpired())
			G_GAME_GLOBAL.setManagerAndInitializeByTemplate<DiaFirst>();
	}
}
void CubeApp::releaseGame(int x, int y)
{
	mCam->setCubeModelProjection();
	if( (!mProf1.isAi() && mCube->getCurrentPlayer() == 0) ||
		(!mProf2.isAi() && mCube->getCurrentPlayer() == 1))
	{
		//if mouse has not moved too far from original position
		if(mMouse.getChangeNorm() < 10)
		{
			//if(mCube->getCurMoveType() == 0)
			{
				glClear(GL_COLOR_BUFFER_BIT);
				unsigned face = mDraw->drawSelectionFaces(mCube->getCurrentFace(),x,y,*mCam);
				if(face == 13 && mCube->getCurMoveType() == 1)
				{
					//TODO play a sound
					G_SOUND_MANAGER.getSound(SOUND_HIGHLIGHT)->play();
					mCube->undoLastMove();
				}
				else if(face != 13 && mCube->getLastMove() != face)
				{
					if( mCube->getCurMoveType() == 1 )
						mCube->undoLastMove();
					mCube->attempMakeMove(face);
				}
			}
		}
		QuTurnDirections dir = mMouse.mouseReleased(x,y);
		if(dir != IDENTITY_DIRECTION && !mCube->attemptTurnToFace(dir))
		{
			G_SOUND_MANAGER.getSound(SOUND_ERROR)->play();
			float mag = 2000;
			switch(dir)
			{
			case LEFT:
				mCam->addImpulse(-mag,0);
				break;
			case RIGHT:
				mCam->addImpulse(mag,0);
				break;
			case UP:
				mCam->addImpulse(0,-mag);
				break;
			case DOWN:
				mCam->addImpulse(0,mag);
				break;
			default:
				break;
			}
		}
		mCam->rotateInDirection(mCube->getAndResetLastTurnDirection());
	}
}
void CubeApp::pressGame(int x, int y)
{
	mCam->setCubeModelProjection();
	glClear(GL_COLOR_BUFFER_BIT);
	if(mDraw->drawSelectionFaces(mCube->getCurrentFace(),x,y,*mCam) != 13)
		mCam->addImpulse(SCREEN_WIDTH/2 - x,SCREEN_HEIGHT/2 - y);
}

void CubeApp::update()
{
	updateGame();
	draw();
}

void CubeApp::draw(){
	glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
	drawGame();
}

void CubeApp::keyPressed(int key){
	mAccel.keyPressed(key);
}

void CubeApp::keyReleased(int key){
	if(key == s3eKeyAbsBSK)
		G_GAME_GLOBAL.setManagerAndInitializeByTemplate<DiaFirst>();
	mAccel.keyReleased(key);
}
 
void CubeApp::mousePressed(int x, int y, int button){
	mMouse.mousePressed(x,y);
	pressGame(x,y);
}

void CubeApp::mouseReleased(int x, int y, int button){
	releaseGame(x,y);
}

void CubeApp::mouseMoved(int x, int y)
{
	mMouse.mouseMoved(x,y);
}


//--------------------------------------------------------------
void CubeApp::windowResized(int w, int h){
	//reset screen center
}

void CubeApp::tiltUpdate()
{
	float y = s3eAccelerometerGetY()/1000.0;
	float x = s3eAccelerometerGetX()/1000.0;
	if(s3eSurfaceGetInt(S3E_SURFACE_DEVICE_BLIT_DIRECTION) == S3E_SURFACE_BLIT_DIR_ROT180)
	{
		x*=-1; y*=-1;
	}
	mCam->setTiltTarget(y,x,s3eAccelerometerGetZ()/1000.0);	
	mCam->adjustTiltTarget(-mMouse.getChange().x/30.,mMouse.getChange().y/40.);


	//mCam->setTiltTarget(mAccel.getY(),mAccel.getX());
	//mCam->adjustTiltTarget(-mMouse.getChange().x/100.,mMouse.getChange().y/200.);
}
