#include "QuConstants.h"
#include "QuCubeConstants.h"
#include "QuExceptions.h"
#include "QuCube.h"

//LAME
#include "QuMaterials.h"
//list<int> QuLights::globalAvail = list<int>();

//mew
#include "QuGlobals.h"

bool QuCube::highlightMove(unsigned aSubCube)
{
	//TODO check if game state is correct
	if(getCurMoveType() != 0)
		return false;
	unHighlightCube();
	QuMoveImplications imp = getCubeImplication(aSubCube, getCurrentPlayer());
	if(isOkayToHighlight(imp,getCurrentPlayer()))
	{
		setSubCubeState(aSubCube,playerToHighlightState(getCurrentPlayer()));
		if(aSubCube != 13)
			G_SOUND_MANAGER.getSound(SOUND_HIGHLIGHT)->play();
		return true;
	}
	return false;
}

bool QuCube::isMoveHighlighted(unsigned aSubCube)
{
	return (getSubCubeState(aSubCube) == P1_HIGHLIGHT || getSubCubeState(aSubCube) == P2_HIGHLIGHT);
}

bool QuCube::attempMakeMove(unsigned aSubCube)
{
	if(getCurMoveType() != 0)
		return false;
	QuMoveImplications imp = getCubeImplication(aSubCube, getCurrentPlayer());
	if(isOkayToMove(imp,getCurrentPlayer()))
	{
		setSubCubeState(aSubCube,playerToOwnerState(getCurrentPlayer()));
		mLastMoveMade = aSubCube;
		if(computeWinningFaces(aSubCube))
		{
			if(getCurrentPlayer() == 0)
				mCurState = P1_WINNER;
			else
				mCurState = P2_WINNER;

			makeFaceTurnByFace(computeWinningFace(getCurrentFace()));
			G_SOUND_MANAGER.getSound(SOUND_WINNER)->play();
			cout << "player " << getCurrentPlayer()+1 << " has won" << endl;
			return true;
		}
		G_SOUND_MANAGER.getSound(SOUND_SELECT)->play();
		advanceState();
		return true;
	}
	return false;
}

bool QuCube::attemptTurnToFace(unsigned aFace)
{
	if(getCurMoveType() != 1)
		return false;
	QuMoveImplications imp = getFaceImplication(aFace, getCurrentPlayer());
	if(isOkayToTurn(imp,getCurrentPlayer()))
	{
		makeFaceTurnByFace(aFace);
		advanceState();
		G_SOUND_MANAGER.getSound(SOUND_NEXT_TURN)->play();
		//printCurrentFace();
		return true;
	}
	return false;
}

bool QuCube::attemptTurnToFace(QuTurnDirections aDir)
{
	return attemptTurnToFace(getFaceInDirection(aDir));
}

void QuCube::undoLastMove()
{
	setSubCubeState(mLastMoveMade,NEUTRAL);
	mLastMoveMade = 13;
	revertState();
}