#pragma once
#include <cmath>
template <typename T>
struct CubePoint
{
	T x,y;
	CubePoint(T ax, T ay)
	{
		x = ax;
		y = ay;
	}
};

struct CubeVector3 {
	virtual ~CubeVector3(){}

	CubeVector3( float _x=0.0f, float _y=0.0f, float _z=0.0f ) {
		x = _x;
		y = _y;
		z = _z;
	}

	CubeVector3( const CubeVector3 & pnt){
		x = pnt.x;
		y = pnt.y;
		z = pnt.z;
	}

	void set(float _x, float _y, float _z = 0){
		x = _x;
		y = _y;
		z = _z;
	}


	//------ Operators:

	//Negative
	CubeVector3 operator-() const {
		return CubeVector3( -x, -y, -z );
	}

	//equality
	bool operator==( const CubeVector3& pnt ) {
		return (x == pnt.x) && (y == pnt.y) && (z == pnt.z);
	}

	//inequality
	bool operator!=( const CubeVector3& pnt ) {
		return (x != pnt.x) || (y != pnt.y) || (z != pnt.z);
	}

	//Set
	CubeVector3 & operator=( const CubeVector3& pnt ){
		x = pnt.x;
		y = pnt.y;
		z = pnt.z;
		return *this;
	}

	CubeVector3 & operator=( const float& val ){
		x = val;
		y = val;
		z = val;
		return *this;
	}

	// Add
	CubeVector3 operator+( const CubeVector3& pnt ) const {
		return CubeVector3( x+pnt.x, y+pnt.y, z+pnt.z );
	}

	CubeVector3 operator+( const float& val ) const {
		return CubeVector3( x+val, y+val, z+val );
	}

	CubeVector3 & operator+=( const CubeVector3& pnt ) {
		x+=pnt.x;
		y+=pnt.y;
		z+=pnt.z;
		return *this;
	}

	CubeVector3 & operator+=( const float & val ) {
		x+=val;
		y+=val;
		z+=val;
		return *this;
	}

	// Subtract
	CubeVector3 operator-(const CubeVector3& pnt) const {
		return CubeVector3( x-pnt.x, y-pnt.y, z-pnt.z );
	}

	CubeVector3 operator-(const float& val) const {
		return CubeVector3( x-val, y-val, z-val);
	}

	CubeVector3 & operator-=( const CubeVector3& pnt ) {
		x -= pnt.x;
		y -= pnt.y;
		z -= pnt.z;
		return *this;
	}

	CubeVector3 & operator-=( const float & val ) {
		x -= val;
		y -= val;
		z -= val;
		return *this;
	}

	// Multiply
	CubeVector3 operator*( const CubeVector3& pnt ) const {
		return CubeVector3( x*pnt.x, y*pnt.y, z*pnt.z );
	}

	CubeVector3 operator*(const float& val) const {
		return CubeVector3( x*val, y*val, z*val);
	}

	CubeVector3 & operator*=( const CubeVector3& pnt ) {
		x*=pnt.x;
		y*=pnt.y;
		z*=pnt.z;
		return *this;
	}

	CubeVector3 & operator*=( const float & val ) {
		x*=val;
		y*=val;
		z*=val;
		return *this;
	}


	// Divide
	CubeVector3 operator/( const CubeVector3& pnt ) const {
		return CubeVector3( pnt.x!=0 ? x/pnt.x : x , pnt.y!=0 ? y/pnt.y : y, pnt.z!=0 ? z/pnt.z : z );
	}

	CubeVector3 operator/( const float &val ) const {
		if( val != 0){
			return CubeVector3( x/val, y/val, z/val );
		}
		return CubeVector3(x, y, z );
	}

	CubeVector3& operator/=( const CubeVector3& pnt ) {
		pnt.x!=0 ? x/=pnt.x : x;
		pnt.y!=0 ? y/=pnt.y : y;
		pnt.z!=0 ? z/=pnt.z : z;

		return *this;
	}

	CubeVector3& operator/=( const float &val ) {
		if( val != 0 ){
			x /= val;
			y /= val;
			z /= val;
		}

		return *this;
	}

	float length()
	{
		return std::sqrt(x*x + y*y + z*z);
	}
	void normalize()
	{
		(*this)/=length();
	}

	// union allows us to access the coordinates through
	// both an array 'v' and 'x', 'y', 'z' member varibles
	union  {
		struct {
			float x;
			float y;
			float z;
		};
		float v[3];
	};
};
template <typename T>
struct QuRectangle
{
	T x,y,w,h;
	QuRectangle(T ax, T ay, T aw, T ah)
	{
		x = ax;
		y = ay;
		w = aw;
		h = ah;
	}
	bool doesContainPoint(CubePoint<T> pt)
	{
		if(pt.x > x+w || pt.x < x)
			return false;
		if(pt.y > y+h || pt.y < y)
			return false;
		return true;
	}
	bool doesContainPoint(int ax, int ay)
	{
		return doesContainPoint(CubePoint<int>(ax,ay));
	}
};
