#pragma once

#include "QuAiPartial.h"
#include "QuConstants.h"
#include "QuCubeConstants.h"
#include "QuCube.h"
#include <list>

//new
#include "QuClutils.h"
#include "QuUtils.h"
#include "CubeMaxMin.h"

#define MIN(a,b)    ( (a) < (b) ? (a) : (b) )

using namespace std;
using namespace QUAIPARTIAL;




class QuNewAiPlayer
{
	int mC;
	unsigned mPlayer;
	unsigned mDiff;
	QuTimer mHighTimer;
	QuTimer mTurnTimer;
	QuTimer mIterateTimer;
	char mCube[27];
	AntonAi::IterativeStrategy * mData;
	AntonAi::Move mv;
public:
	QuNewAiPlayer(unsigned aPlayer, unsigned aDiff)
	{
		mData = NULL;
		mDiff = aDiff;
		mPlayer = aPlayer;
		mC = 0;
		mHighTimer.setTargetAndReset(3);
		mTurnTimer.setTargetAndReset(25);
		mIterateTimer.setTargetAndReset(1);
	}
	~QuNewAiPlayer()
	{
		if(mData)
			delete mData;
	}
	AntonAi::AiDifficulty convertToAiDiff(int aDiff)
	{
		switch(aDiff)
		{
		case 0: return AntonAi::AD_EASY; break;
		case 1: return AntonAi::AD_MEDIUM; break;
		case 2: return AntonAi::AD_HARD; break;
		default: quAssert(false); return AntonAi::AD_EASY; break;
		}
	}
	void runAi()
	{
		mData->MultiStep(MAX_AI_DEPTH);
	}
	void update(QuCube & cube)
	{
		//if someone has won, we need do nothing
		if(cube.getCurrentPlayer() != mPlayer || cube.getCurMoveType() == 2)
			return;
		
		if(mData == NULL)
		{
			cube.makeNewAiCube(mCube);
			//cout << "player is " << mPlayer << " diff is " << mDiff << endl;
			AntonAi::Game game(cube.getCurrentFace(),cube.getCurrentNewAiPlayer());
			memmove(game.arrCube, mCube, 27*sizeof(char));
			mData = new AntonAi::IterativeStrategy(game,convertToAiDiff(mDiff));
			switch(mDiff)
			{
				case 0: mHighTimer.setTargetAndReset(3); mIterateTimer.setTargetAndReset(1); break;
				case 1: mHighTimer.setTargetAndReset(6); mIterateTimer.setTargetAndReset(1); break;
				case 2: mHighTimer.setTargetAndReset(10); mIterateTimer.setTargetAndReset(2); break;
				default: quAssert(false); mHighTimer.setTargetAndReset(3); break;
			}
		}

		if(mC == 0)
		{
			runAi();
			if(mTurnTimer.isExpired())
			{
				mC = 1;
				mTurnTimer.reset();
			}
			else
				mTurnTimer.update();
		}
		else if(mC < 10)
		{
			runAi();
			if(mHighTimer.isExpired())
			{
				while(mC < 10)
				{

					if(cube.highlightMove(FACE_TO_SUBCUBES[cube.getCurrentFace()][mC-1]))
					{
						mHighTimer.reset();
						mC++;
						break;
					}
					mC++;
				}
				if(mC == 10)
				{
					mIterateTimer++;
					if(!mIterateTimer.isExpired())
						mC = 1;
				}
			}
			mHighTimer.update();
		}
		else if (mC == 10)
		{
			runAi();
			if(mTurnTimer.isExpired())
			{
				mTurnTimer.reset();
				mC = 11;
			}
			else
			{
				cube.unHighlightCube();
			}
			mTurnTimer.update();
		}
		else if (mC == 11)
		{
			runAi();
			if(mHighTimer.isExpired())
			{
				mv = mData->GetMove();
				cube.attempMakeMove(FACE_TO_SUBCUBES[cube.getCurrentFace()][mv.nMoveSquare]);
				mHighTimer.reset();
				mC = 12;
			}
			mHighTimer.update();
		}
		else
		{
			if(mTurnTimer.isExpired())
			{
				if(mv.nTurnSide == -1 || !cube.attemptTurnToFace(mv.nTurnSide))
					cube.makeARandomTurn();
				mTurnTimer.reset();
				mC = 0;

				delete mData;
				mData = NULL;
			}
			mTurnTimer.update();
		}
		
	}
private:
};






class QuAiPlayer
{
	int mC;
	unsigned mPlayer;
	unsigned mDiff;
	QuTimer mHighTimer;
	QuTimer mTurnTimer;
	char mCube[27];
	AiData * mData;
	QuMove mv;
public:
	QuAiPlayer(unsigned aPlayer, unsigned aDiff)
	{
		mData = NULL;
		mDiff = aDiff;
		mPlayer = aPlayer;
		mC = 0;
		mHighTimer.setTargetAndReset(3);
		mTurnTimer.setTargetAndReset(25);
	}
	~QuAiPlayer()
	{
		if(mData)
			delete mData;
	}
	void update(QuCube & cube)
	{
		//if someone has won, we need do nothing
		if(cube.getCurrentPlayer() != mPlayer || cube.getCurMoveType() == 2)
			return;
		
		if(mData == NULL)
		{
			cube.makeAiCube(mCube);
			//cout << "player is " << mPlayer << " diff is " << mDiff << endl;
			mData = new AiData(mCube,cube.getCurrentFace(),cube.getCurrentAiPlayer(),mDiff);
			if(mDiff > 3)
				mHighTimer.setTargetAndReset(8);
			else if(mDiff < 10)
				mHighTimer.setTargetAndReset(5);
			else
				mHighTimer.setTargetAndReset(3);
		}

		if(mC == 0)
		{
			mData->Calculate(MAX_AI_DEPTH);

			if(mTurnTimer.isExpired())
			{
				mC = 1;
				mTurnTimer.reset();
			}
			else
				mTurnTimer.update();
		}
		else if(mC < 10)
		{
			mData->Calculate(MAX_AI_DEPTH);
			if(mHighTimer.isExpired())
			{
				while(mC < 10)
				{
					if(cube.highlightMove(FACE_TO_SUBCUBES[cube.getCurrentFace()][mC-1]))
					{
						mHighTimer.reset();
						mC++;
						break;
					}
					mC++;
				}
			}
			mHighTimer.update();
		}
		else if (mC == 10)
		{
			mData->Calculate(MAX_AI_DEPTH);
			if(mTurnTimer.isExpired())
			{
				mTurnTimer.reset();
				mC = 11;
			}
			else
			{
				cube.unHighlightCube();
			}
			mTurnTimer.update();
		}
		else if (mC == 11)
		{
			mData->Calculate(MAX_AI_DEPTH);
			if(mHighTimer.isExpired())
			{
				mv = mData->GetMove();
				cube.attempMakeMove(FACE_TO_SUBCUBES[cube.getCurrentFace()][mv.nMove]);
				mHighTimer.reset();
				mC = 12;
			}
			mHighTimer.update();
		}
		else
		{
			if(mTurnTimer.isExpired())
			{
				if(mv.nFace == -1 || !cube.attemptTurnToFace(mv.nFace))
				{
					cube.makeARandomTurn();
				}
				mTurnTimer.reset();
				mC = 0;

				delete mData;
				mData = NULL;
			}
			mTurnTimer.update();
		}
		
	}
private:
};

class QuPlayerProfile
{
	bool isPlayerAi;
	int mPlayer;
	QuNewAiPlayer * mAi;
public:
	QuPlayerProfile(int aPlayer)
	{
		mPlayer = aPlayer;
		isPlayerAi = false;
	}
	~QuPlayerProfile()
	{
		if(isPlayerAi)
			delete mAi;
	}
	void reset()
	{
		if(isPlayerAi)
			delete mAi;
		isPlayerAi = false;
	}
	bool isAi()
	{
		return isPlayerAi;
	}
	void createAi(int diff)
	{
		isPlayerAi = true;
		assert(diff >= 0);
		mAi = new QuNewAiPlayer(mPlayer,diff);
	}
	QuNewAiPlayer & getAi()
	{
		assert(isPlayerAi);
		return *mAi;
	}
	
};

