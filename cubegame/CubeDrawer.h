#pragma once
#include <cmath>
#include "QuMaterials.h"
#include "subdatatypes.h"
#include "QuDrawer.h"
#include "CubeModels.h"

class CubeDrawer
{
	int steps;
	QuDrawObject mFaceDrawObject;
	QuDrawObject mDivetDrawObject;
	QuDrawObject mSphereDrawObject;
	QuDrawObject mArrowDrawObject;

	QuColorableDrawObject<unsigned> mSelectionDrawObject;
	QuMaterial mRedMat, mBlueMat, mNeutralMat, mRedHighMat, mBlueHighMat;
	QuMaterial mBFN, mBFR, mBFB, mBDN, mBDR, mBDB, mFFN, mFFR, mFFB, mFDN, mFDR, mFDB;
	QuMaterial mArrowMat;

	bool mFrontFacing [6];

	QuTimer mGameTimer;

public:
	CubeDrawer()
	{
		initialize();
	}

	~CubeDrawer() 
	{
	}

private:
	void initModelsAndLights()
	{
		//MODELS
		steps = 28;
		float * texData;
		float trans[3] = {0,0.5,0.5};

		//string image = "images/cube_texture.png";
		string image = "images/cube_texture_clean.png";

		mFaceDrawObject.setCount(steps*2);
		mFaceDrawObject.loadVerticesAndGenerateNormals(QuFunctionTesselator::basicTesselator(QuFunctionTesselator::square<1,1>,QuFunctionTesselator::circle<1,3,0,1>,steps,QuFunctionTesselator::squarePoi()));
		texData = QuFunctionTesselator::basicTesselator(QuFunctionTesselator::square<1,1>,QuFunctionTesselator::circle<1,3,0,1>,steps,QuFunctionTesselator::squarePoi());
		operateAndReplaceVector<3>(texData,steps*2,1,trans);
		mFaceDrawObject.loadImage(image,texData,3,1);;
		//printMatrix<float>(mFaceDrawObject.mN.mData,steps*2,3);
		
		mDivetDrawObject.setCount(steps*2);
		mDivetDrawObject.loadVerticesAndGenerateNormals(QuFunctionTesselator::basicTesselator(QuFunctionTesselator::circle<1,3,0,1>,QuFunctionTesselator::circle<1,4,1,6>,steps,QuFunctionTesselator::squarePoi()));
		texData = QuFunctionTesselator::basicTesselator(QuFunctionTesselator::circle<1,3,0,1>,QuFunctionTesselator::circle<1,4,1,6>,steps,QuFunctionTesselator::squarePoi());
		operateAndReplaceVector<3>(texData,steps*2,1,trans);
		mDivetDrawObject.loadImage(image,texData,3,1);

		mArrowDrawObject.setCount(3*3*12);
		mArrowDrawObject.setDrawType(GL_TRIANGLES);
		float * verts = new float[3*3*12];
		memcpy(verts,&ARROW_TRIANGLE_COORDS[0][0][0],sizeof(float)*3*3*12);
		mArrowDrawObject.loadVerticesAndGenerateNormals(verts);
		mArrowDrawObject.loadImage(image,cpcpcpVector<float>(&STANDARD_TRIANGLE_UV[0][0],6,12));


		int sub = 2;
		int iter = 1;
		for(int i = 0; i < sub; i++)
			iter*=4;
		//mSphereDrawObject.setCount(8*3*iter);
		mSphereDrawObject.setCount(4*3*iter);
		float * v = NULL;
		float * n = NULL;
		float * t = NULL;	//lol this is leaked but who cares
		//generateIcosphere(v,n,t,sub);
		generatePartialIcosphere(v,n,t,sub);
		mSphereDrawObject.loadVertices(v);
		mSphereDrawObject.loadNormals(n);
		//mSphereDrawObject.loadImage("images/noise.jpg",t);
		mSphereDrawObject.setDrawType(GL_TRIANGLES);

		float * faceData = new float[4*3];
		memcpy(faceData,STANDARD_FACE_COORDS,sizeof(float)*4*3);
		mSelectionDrawObject.setCount(4);
		mSelectionDrawObject.loadVertices(faceData);
		for(unsigned i = 0; i < 9; i++)
			mSelectionDrawObject.addColor(i,cpcpcpVector<float>(SELECTION_INDEX_TO_COLOR[i],4,4),4);

		//LIGHTS
		mRedMat = QuMaterialPrefabs::getMaterial("RED");
		mBlueMat = QuMaterialPrefabs::getMaterial("BLUE");
		mRedHighMat = QuMaterialPrefabs::getMaterial("REDHIGH");
		mBlueHighMat = QuMaterialPrefabs::getMaterial("BLUEHIGH");
		mNeutralMat = QuMaterialPrefabs::getMaterial("NEUTRAL");
		
		mArrowMat = QuMaterialPrefabs::getMaterial("ARROW");

		//notation <F/B><D/F><R/N/B> front/back, divet/face, red/blue/neutral
		mBFN = QuMaterialPrefabs::getMaterial("BFN");
		mBFR = QuMaterialPrefabs::getMaterial("BFR");
		mBFB = QuMaterialPrefabs::getMaterial("BFB");
		mBDN = QuMaterialPrefabs::getMaterial("BDN");
		mBDR = QuMaterialPrefabs::getMaterial("BDR");
		mBDB = QuMaterialPrefabs::getMaterial("BDB");
		mFFN = QuMaterialPrefabs::getMaterial("FFN");
		mFFR = QuMaterialPrefabs::getMaterial("FFR");
		mFFB = QuMaterialPrefabs::getMaterial("FFB");
		mFDN = QuMaterialPrefabs::getMaterial("FDN");
		mFDR = QuMaterialPrefabs::getMaterial("FDR");
		mFDB = QuMaterialPrefabs::getMaterial("FDB");
	}
	void initialize()
	{
		initModelsAndLights();
	}

	void drawSubCubeFace(unsigned aSubCube, unsigned aFace, unsigned aState,  QuMaterial & facemat, QuMaterial & divetmat, float aScale = 1)
	{
		//QuVector3 norm = faceToNormal(aFace);
		glPushMatrix();
		//translate to correct place
		GLint * pos = SUBCUBE_TO_SPATIAL_POSITION[aSubCube];
		glTranslatef(pos[0],pos[1],pos[2]);
		//calculate rotations and scaling
		int * axis = VECTOR_INDEX_TO_AXIS[FACE_TO_AXIS_ANGLE[aFace][0]];
		int angle = FACE_TO_AXIS_ANGLE[aFace][1];
		glScalef(aScale,aScale,aScale);
		glRotatef(angle,axis[0],axis[1],axis[2]);
		//compensate for scale effect on x axis
		//glTranslatef(-1/2.0f/aScale,0,0);
		glTranslatef(-1/2.0f,0,0);
		
		facemat.enable();
		mFaceDrawObject.enable();
		mFaceDrawObject.draw();
		mFaceDrawObject.disable();
		
		divetmat.enable();
		mDivetDrawObject.enable();
		mDivetDrawObject.draw();
		mDivetDrawObject.disable();

		glPopMatrix();
	}

	void drawPartialSphere(unsigned aSubCube, unsigned aFace, float aScale)
	{
		glPushMatrix();
		aScale*=0.5;
		GLint * pos = SUBCUBE_TO_SPATIAL_POSITION[aSubCube];
		glTranslatef(pos[0],pos[1],pos[2]);
		int * axis = VECTOR_INDEX_TO_AXIS[FACE_TO_AXIS_ANGLE[aFace][0]];
		int angle = FACE_TO_AXIS_ANGLE[aFace][1];
		glScalef(aScale,aScale,aScale);
		glRotatef(angle,axis[0],axis[1],axis[2]);
		mSphereDrawObject.enable();
		mSphereDrawObject.draw();
		mSphereDrawObject.disable();
		glPopMatrix();
	}
	/*use only for full sphere model
	void drawSphere(unsigned aSubCube, float aScale)
	{
		aScale*=0.5;
		glPushMatrix();
		//translate to correct place
		GLint * pos = SUBCUBE_TO_SPATIAL_POSITION[aSubCube];
		glTranslatef(pos[0],pos[1],pos[2]);
		glScalef(aScale,aScale,aScale);
		//glutSolidSphere(0.45f,20,20);
		mSphereDrawObject.enable();
		mSphereDrawObject.draw();
		mSphereDrawObject.disable();
		glPopMatrix();
	}*/
public:
	
	unsigned drawSelectionFaces(unsigned aFace, int x, int y, QuCamera & aCam)
	{
		
		//glPushAttrib(GL_LIGHTING_BIT | GL_CURRENT_BIT | GL_DEPTH_BUFFER_BIT);
		glDepthMask(GL_FALSE);
		glDisable(GL_DEPTH_TEST);
		glDisable(GL_LIGHTING);
		//set buffer NAH, we draw on the main buffer and shit gets cleared for us. it's cool
		const unsigned * subCubes = FACE_TO_SUBCUBES[aFace]; 
		for(unsigned i = 0; i < 9; i++)
		{
			glPushMatrix();
			GLint * pos = SUBCUBE_TO_SPATIAL_POSITION[subCubes[i]];
			glTranslatef(pos[0],pos[1],pos[2]);
			int * axis = VECTOR_INDEX_TO_AXIS[FACE_TO_AXIS_ANGLE[aFace][0]];
			int angle = FACE_TO_AXIS_ANGLE[aFace][1];
			glRotatef(angle,axis[0],axis[1],axis[2]);
			glTranslatef(-1/2.0f,0,0);
			mSelectionDrawObject.setKey(i);
			mSelectionDrawObject.autoDraw();
			glPopMatrix();
		}
		glEnable(GL_DEPTH_TEST);
		glDepthMask(GL_TRUE);
		glEnable(GL_LIGHTING);
		//glPopAttrib();

		
		GLubyte r[4];
		//cout << "click at " << x << " " << y << endl;
        glReadPixels(x,SCREEN_HEIGHT-y,1,1,GL_RGBA,GL_UNSIGNED_BYTE,r);
		//cout << "color selected " << (int)r[0] << " " << (int)r[1] << " " << (int)r[2] << " " << (int)r[3] << endl;
        for(int i = 0; i < 9; i++)
        {
			if(r[1] > 0 || r[2] > 0)
				continue;
			//cout << "touching difference " << r[0]-SELECTION_INDEX_TO_COLOR_FOR_COMPARISON[i][0] << endl;
            if(quAbs<float>(r[0]-SELECTION_INDEX_TO_COLOR_FOR_COMPARISON[i][0]) < SELECTION_TOLERANCE)
                return subCubes[i];
        }
		return 13;	//return center if no cube selected
	}
	void computeFrontFacingFaces(QuCamera & cam)
	{
		//float * vm3 = cam.getInverseRotation().convertToM3();

		float vm[ 16 ]; 
		glGetFloatv( GL_MODELVIEW_MATRIX, vm );
		float * vm3 = convertM4toM3(vm);
		float * l = transformVectorWithMatrix<float,3>(vm3,FRONT_FACING_VECTOR);

		for(int i = 0; i < 6; i++)
		{
			float n[3];
			for(int j = 0; j < 3; j++)
				n[j] = FACE_TO_NORMAL[i][j]; 
			if(dot<float,3>(n,l) > 0)
				mFrontFacing[i] = true;
			else
				mFrontFacing[i] = false;			
		}
		delete [] l;
		delete [] vm3;
	}


	void drawCube(QuCube & aCube, int aFrontFace)
	{

		

		/*
		glEnable(GL_BLEND);
		glDisable(GL_DEPTH_TEST);
		glBlendFunc(GL_SRC_ALPHA,GL_DST_ALPHA);
		*/
		//glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
		//glEnable(GL_POLYGON_SMOOTH);

		 

		glEnable(GL_NORMALIZE);
		glEnable(GL_DEPTH_TEST);
		glLightfv(GL_LIGHT1,GL_DIFFUSE,DEFAULT_LIGHT_DIFFUSE);
		glLightfv(GL_LIGHT1,GL_SPECULAR,DEFAULT_LIGHT_SPECULAR);
		glLightfv(GL_LIGHT1,GL_AMBIENT,DEFAULT_LIGHT_AMBIENT);
		glEnable(GL_LIGHTING);
		glEnable(GL_LIGHT1);

		

		for(int i = 0; i < 27; i++)
		{
			for(int j = 0; j < 6; j++)
			{
				QuMaterial * facemat;
				QuMaterial * divetmat;
				if(SUBCUBE_TO_EXPOSED_FACETS[i][j] && mFrontFacing[j])
				{
					switch(aCube.getSubCubeState(i))
					{
					case P1_HIGHLIGHT:
					case P1_OWN:
						if(aFrontFace == j) 
						{
							divetmat = &mFDR;
							facemat = &mFFR;
						}
						else
						{
							divetmat = &mBDR;
							facemat = &mBFR;
						}
						break;
					case P2_OWN:
					case P2_HIGHLIGHT:
						if(aFrontFace == j) 
						{
							divetmat = &mFDB;
							facemat = &mFFB;
						}
						else
						{
							divetmat = &mBDB;
							facemat = &mBFB;
						}
						break;
					default:
						if(aFrontFace == j) 
						{
							divetmat = &mFDN;
							facemat = &mFFN;
						}
						else
						{
							divetmat = &mBDN;
							facemat = &mBFN;
						}
						break;
					}
					drawSubCubeFace(i,j,0,*facemat,*divetmat,1.0f);
				}
			}

			switch(aCube.getSubCubeState(i))
			{
			case NEUTRAL:
				mNeutralMat.enable();
				break;
			case P1_OWN:
				mRedMat.enable();
				break;
			case P2_OWN:
				mBlueMat.enable();
				break;
			case P1_HIGHLIGHT:
				mRedHighMat.enable();
				break;
			case P2_HIGHLIGHT:
				mBlueHighMat.enable();
				break;
			case P1_FLASH: 
				//TODO put in a frame counter function
				if(mGameTimer.getTimeSinceStart()%2)
					mRedHighMat.enable();
				else mRedMat.enable();
				break;
			case P2_FLASH:
				if(mGameTimer.getTimeSinceStart()%2)
					mBlueHighMat.enable();
				else mBlueMat.enable();
				break;
			default:
				mNeutralMat.enable();
				break;
			}

			for(int j = 0; j < 6; j++)
			{
				if(SUBCUBE_TO_EXPOSED_FACETS[i][j] && mFrontFacing[j])
				{ 
					drawPartialSphere(i,j,.85f); 
				}
			}
		}
		

		/*
		glDisable(GL_DEPTH_TEST);
		glEnable(GL_BLEND);
		//glBlendFunc(GL_SRC_ALPHA,GL_DST_ALPHA);
		glPushMatrix();
		mArrowDrawObject.loadColor(QuColor(1,1,0,0.5));
		//mArrowMat.enable();
		glTranslatef(0,0,3);
		mArrowDrawObject.autoDraw();
		glPopMatrix();
		glEnable(GL_DEPTH_TEST);
		glDisable(GL_BLEND);
		*/






		mGameTimer++;
	}
	
};