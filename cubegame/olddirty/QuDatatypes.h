#pragma once
#include <cmath>
struct QuVector3 {
	virtual ~QuVector3(){}

	QuVector3( float _x=0.0f, float _y=0.0f, float _z=0.0f ) {
		x = _x;
		y = _y;
		z = _z;
	}

	QuVector3( const QuVector3 & pnt){
		x = pnt.x;
		y = pnt.y;
		z = pnt.z;
	}

	void set(float _x, float _y, float _z = 0){
		x = _x;
		y = _y;
		z = _z;
	}


	//------ Operators:

	//Negative
	QuVector3 operator-() const {
		return QuVector3( -x, -y, -z );
	}

	//equality
	bool operator==( const QuVector3& pnt ) {
		return (x == pnt.x) && (y == pnt.y) && (z == pnt.z);
	}

	//inequality
	bool operator!=( const QuVector3& pnt ) {
		return (x != pnt.x) || (y != pnt.y) || (z != pnt.z);
	}

	//Set
	QuVector3 & operator=( const QuVector3& pnt ){
		x = pnt.x;
		y = pnt.y;
		z = pnt.z;
		return *this;
	}

	QuVector3 & operator=( const float& val ){
		x = val;
		y = val;
		z = val;
		return *this;
	}

	// Add
	QuVector3 operator+( const QuVector3& pnt ) const {
		return QuVector3( x+pnt.x, y+pnt.y, z+pnt.z );
	}

	QuVector3 operator+( const float& val ) const {
		return QuVector3( x+val, y+val, z+val );
	}

	QuVector3 & operator+=( const QuVector3& pnt ) {
		x+=pnt.x;
		y+=pnt.y;
		z+=pnt.z;
		return *this;
	}

	QuVector3 & operator+=( const float & val ) {
		x+=val;
		y+=val;
		z+=val;
		return *this;
	}

	// Subtract
	QuVector3 operator-(const QuVector3& pnt) const {
		return QuVector3( x-pnt.x, y-pnt.y, z-pnt.z );
	}

	QuVector3 operator-(const float& val) const {
		return QuVector3( x-val, y-val, z-val);
	}

	QuVector3 & operator-=( const QuVector3& pnt ) {
		x -= pnt.x;
		y -= pnt.y;
		z -= pnt.z;
		return *this;
	}

	QuVector3 & operator-=( const float & val ) {
		x -= val;
		y -= val;
		z -= val;
		return *this;
	}

	// Multiply
	QuVector3 operator*( const QuVector3& pnt ) const {
		return QuVector3( x*pnt.x, y*pnt.y, z*pnt.z );
	}

	QuVector3 operator*(const float& val) const {
		return QuVector3( x*val, y*val, z*val);
	}

	QuVector3 & operator*=( const QuVector3& pnt ) {
		x*=pnt.x;
		y*=pnt.y;
		z*=pnt.z;
		return *this;
	}

	QuVector3 & operator*=( const float & val ) {
		x*=val;
		y*=val;
		z*=val;
		return *this;
	}


	// Divide
	QuVector3 operator/( const QuVector3& pnt ) const {
		return QuVector3( pnt.x!=0 ? x/pnt.x : x , pnt.y!=0 ? y/pnt.y : y, pnt.z!=0 ? z/pnt.z : z );
	}

	QuVector3 operator/( const float &val ) const {
		if( val != 0){
			return QuVector3( x/val, y/val, z/val );
		}
		return QuVector3(x, y, z );
	}

	QuVector3& operator/=( const QuVector3& pnt ) {
		pnt.x!=0 ? x/=pnt.x : x;
		pnt.y!=0 ? y/=pnt.y : y;
		pnt.z!=0 ? z/=pnt.z : z;

		return *this;
	}

	QuVector3& operator/=( const float &val ) {
		if( val != 0 ){
			x /= val;
			y /= val;
			z /= val;
		}

		return *this;
	}

	float length()
	{
		return std::sqrt(x*x + y*y + z*z);
	}
	void normalize()
	{
		(*this)/=length();
	}

	// union allows us to access the coordinates through
	// both an array 'v' and 'x', 'y', 'z' member varibles
	union  {
		struct {
			float x;
			float y;
			float z;
		};
		float v[3];
	};
};
