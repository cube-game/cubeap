#pragma once
#include "subdatatypes.h"
#include <cmath>


struct QuMouseDragged
{
	int dx, dy;
	int cx, cy;
	bool isMouseDown;
	QuTimer mTimer;
	float mThreshSquared;
	void setThresh(float aThresh)
	{
		mThreshSquared = aThresh*aThresh;
	}
	QuMouseDragged():mTimer(0,15)
	{
		dx = dy = cx = cy = 0;
		setThresh(25);
		isMouseDown = false;
	}
	float getChangeNorm()
	{
		CubePoint<float> c = getChange();
		return sqrt(c.x*c.x + c.y*c.y);
	}
	CubePoint<float> getChange()
	{
		if(isMouseDown)
			return CubePoint<float>((dx-cx),(dy-cy));
		return CubePoint<float>(0,0);
	}
	QuTurnDirections mouseReleased(int x, int y)
	{
		if(mTimer.isExpired())
			return IDENTITY_DIRECTION;
		isMouseDown = false;
		int deltax = (dx-x);
		int deltay = (dy-y);
		//test if threshold is exceeded
		if(deltax*deltax + deltay*deltay < mThreshSquared)
			return IDENTITY_DIRECTION;
		if(quAbs(deltax) >= quAbs(deltay))
		{
			if(deltax > 0)
				return RIGHT;
			else return LEFT;
		}
		else
		{
			if(deltay > 0)
				return DOWN;
			else return UP;
		}
	}
	void mousePressed(int x, int y)
	{
		mTimer.reset();
		cx = dx = x;
		cy = dy = y;
		isMouseDown = true;
	}
	void mouseDragged(int x, int y)
	{
		cx = x; cy = y;
	}
	void mouseMoved(int x, int y)
	{
		if(isMouseDown)
			mouseDragged(x,y);
	}
	void update()
	{
		mTimer++;
	}
};